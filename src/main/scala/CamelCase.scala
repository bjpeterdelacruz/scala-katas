object CamelCase {
  def toCamelCase(str: String): String = {
    if (str.isEmpty) {
      return ""
    }
    val arr = str.split("[-_]+")
    val firstWord = arr(0).substring(0, 1) + arr(0).substring(1).toLowerCase()
    val rest = arr.drop(1).map(str => str.substring(0, 1).toUpperCase() + str.substring(1).toLowerCase())
    firstWord + rest.mkString("")
  }
}