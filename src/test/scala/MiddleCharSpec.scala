import org.scalatest.flatspec.AnyFlatSpec

class MiddleCharSpec extends AnyFlatSpec {
  it should "return middle character(s) in string" in {
    val tests = List(
      ("test",    "es"),
      ("testing", "t"),
      ("middle",  "dd"),
      ("A",       "A"),
      ("of",      "of")
    )
    tests.foreach {
      case (input, expected) =>
        assert(MiddleChar.middle(input) == expected)
    }
  }
}