object PhoneNumber {
  def createPhoneNumber(numbers: Seq[Int]): String = {
    var phoneNumber = "("
    var idx = 0
    for (num <- numbers) {
      phoneNumber += num
      idx += 1
      if (idx == 3) {
        phoneNumber += ") "
      }
      else if (idx == 6) {
        phoneNumber += "-"
      }
    }
    phoneNumber
  }
}