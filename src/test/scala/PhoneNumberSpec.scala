import org.scalatest.flatspec.AnyFlatSpec

class PhoneNumberSpec extends AnyFlatSpec {
  it should "return correctly formatted phone number" in {
    val tests = List(
      (Seq(1, 2, 3, 4, 5, 6, 7, 8, 9, 0), "(123) 456-7890"),
      (Seq(1, 1, 1, 1, 1, 1, 1, 1, 1, 1), "(111) 111-1111")
    )
    tests.foreach {
      case (input, expected) =>
        assert(PhoneNumber.createPhoneNumber(input) == expected)
    }
  }
}