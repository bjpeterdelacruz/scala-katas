object SortByLastChar {
  def last(s: String): Array[String] = {
    val map = s.split("\\s+").groupBy(s => s.takeRight(1))
    val sorted = map.toSeq.sortWith(_._1 < _._1)
    var listOfStrings: Array[String] = Array()
    sorted.foreach(list => {
      listOfStrings = listOfStrings ++ list._2
    });
    listOfStrings
  }
}