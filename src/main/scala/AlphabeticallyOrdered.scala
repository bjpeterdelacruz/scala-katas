object AlphabeticallyOrdered {
  def isAlphabetic(s: String): Boolean = {
    if (s.length < 2) {
      return true
    }
    val alphabet = "abcdefghijklmnopqrstuvwxyz"
    s.toCharArray.sliding(2).forall(a => alphabet.indexOf(a(0)) <= alphabet.indexOf(a(1)))
  }
}