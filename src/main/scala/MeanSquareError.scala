object MeanSquareError {
  def solution(firstArray: Array[Int], secondArray: Array[Int]): Double = {
    var sum: Double = 0.0
    for ((first, second) <- firstArray zip secondArray) {
      sum += ((first - second) * (first - second))
    }
    sum / firstArray.length.toDouble
  }
}