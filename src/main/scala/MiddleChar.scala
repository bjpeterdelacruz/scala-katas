object MiddleChar {
  def middle(str: String): String = {
    if (str.length < 3) {
      return str
    }
    if (str.length % 2 == 0) {
      val middle = (str.length / 2) - 1
      return str.substring(middle, middle + 2)
    }
    str(str.length / 2).toString
  }
}