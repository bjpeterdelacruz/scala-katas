object AlphabetPosition {
  def alphabetPosition(text: String): String = {
    if (text == null || text.isEmpty) {
      return ""
    }
    val alphabet = "abcdefghijklmnopqrstuvwxyz"
    val characters = text.toLowerCase.filter(character => alphabet.indexOf(character) != -1).toList
    characters.map { character => alphabet.indexOf(character) + 1 }.mkString(" ")
  }
}