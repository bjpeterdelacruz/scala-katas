object PrinterErrors {
  def printerError(s: String): String = s"${s.count(char => char > 'm')}/${s.length}"
}