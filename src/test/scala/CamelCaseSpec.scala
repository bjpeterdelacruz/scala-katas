import org.scalatest.flatspec.AnyFlatSpec

class CamelCaseSpec extends AnyFlatSpec {
  it should "turn strings into camel case" in {
    val tests = List(
      ("", ""),
      ("the_Stealth_Warrior", "theStealthWarrior"),
      ("The_Stealth_Warrior", "TheStealthWarrior")
    )
    tests.foreach {
      case (input, expected) =>
        assert(CamelCase.toCamelCase(input) == expected)
    }
  }
}