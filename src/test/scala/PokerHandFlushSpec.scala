import org.scalatest.flatspec.AnyFlatSpec

class PokerHandFlushSpec extends AnyFlatSpec {
  it should "return true if poker hand is a flush, false otherwise" in {
    val tests = List(
      (List("AS", "3S", "9S", "KS", "4S"), true),
      (List("AD", "4S", "7H", "KC", "5S"), false),
      (List("AD", "4S", "10H", "KC", "5S"), false),
      (List("QD", "4D", "10D", "KD", "5D"), true)
    )
    tests.foreach {
      case (input, expected) =>
        assert(PokerHandFlush.isFlush(input) == expected)
    }
  }
}