object PokerHandFlush {
  def isFlush(cards: List[String]): Boolean = {
    cards != null && cards.size == 5 && cards.sliding(2).forall(card => card.head.takeRight(1) == card(1).takeRight(1))
  }
}