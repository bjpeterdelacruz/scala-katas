import org.scalatest.flatspec.AnyFlatSpec

class MeanSquareErrorSpec extends AnyFlatSpec {
  it should "square difference between values in arrays and return average" in {
    val tests = List(
      (Array(4), Array(1), 9.0),
      (Array(1, 2, 3), Array(4, 5, 6), 9.0),
      (Array(10, 20, 10, 2), Array(10, 25, 5, -2), 16.5),
      (Array(0, -1), Array(-1, 0), 1.0)
    )
    tests.foreach {
      case (input1, input2, expected) =>
        assert(MeanSquareError.solution(input1, input2) == expected)
    }
  }
}