import org.scalatest.flatspec.AnyFlatSpec

class SortedByLastCharSpec extends AnyFlatSpec {
  it should "return a sorted array by last character" in {
    val tests = List(
      ("man i need a taxi up to ubud", Array("a", "need", "ubud", "i", "taxi", "man", "to", "up")),
      ("what time are we climbing up the volcano", Array("time", "are", "we", "the", "climbing", "volcano", "up", "what")),
      ("take me to semynak", Array("take", "me", "semynak", "to")),
      ("massage yes massage yes massage", Array("massage", "massage", "massage", "yes", "yes")),
      ("take bintang and a dance please", Array("a", "and", "take", "dance", "please", "bintang"))
    )
    tests.foreach {
      case (input, expected) =>
        assert(SortByLastChar.last(input) sameElements expected)
    }
  }
}