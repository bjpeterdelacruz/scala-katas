import org.scalatest.flatspec.AnyFlatSpec

class AlphabeticallyOrderedSpec extends AnyFlatSpec {
  it should "be true if alphabetically ordered, false otherwise" in {
    val tests = List(
      ("asd", false),
      ("codewars", false),
      ("door", true),
      ("cell", true),
      ("z", true),
      ("", true),
      ("be", true),
      ("xo", false)
    )
    tests.foreach {
      case (input, expected) =>
        assert(AlphabeticallyOrdered.isAlphabetic(input) == expected)
    }
  }
}