import org.scalatest.flatspec.AnyFlatSpec

class PrinterErrorsSpec extends AnyFlatSpec {
  it should "return correct ratio for each string" in {
    val tests = List(
      ("aaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbmmmmmmmmmmmmmmmmmmmxyz", "3/56"),
      ("aaaaaaaaaaaaaaaabbbbbbbbbbbbbbbbbbnnnnnnnnnnnnnnnnxxyyzz", "22/56"),
      ("aaabbbbhaijjjm", "0/14"),
      ("aaaxbbbbyyhwawiwjjjwwm", "8/22")
    )
    tests.foreach {
      case (input, expected) =>
        assert(PrinterErrors.printerError(input) == expected)
    }
  }
}