import org.scalatest.flatspec.AnyFlatSpec

class SumFirstNthTermsSpec extends AnyFlatSpec {
  it should "return sum of series rounded to two decimal places" in {
    val tests = List(
      (1, "1.00"),
      (2, "1.25"),
      (3, "1.39"),
      (4, "1.49"),
      (5, "1.57"),
      (6, "1.63"),
      (7, "1.68"),
      (8, "1.73"),
      (9, "1.77"),
      (15, "1.94"),
      (39, "2.26"),
      (58, "2.40"),
      (0, "0.00")
    )
    tests.foreach {
      case (input, expected) =>
        assert(SumFirstNthTerms.seriesSum(input) == expected)
    }
  }
}