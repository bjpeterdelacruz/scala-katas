import java.util.Locale

object SumFirstNthTerms {
  def seriesSum(n: Int): String = {
    val numbers = 1 to (n * 3) by 3
    val sum = numbers.map(i => 1f / i).sum
    "%.2f".formatLocal(Locale.getDefault, Math.round(sum * 100) / 100f)
  }
}